#!/usr/bin/env dash

# `aha` converts to HTML.
# Remove lines before `<html>`.
HASH="$1"
exec \
    git diff -U6 "$HASH~1..$HASH" | \
    delta --side-by-side --no-gitconfig --paging=never --true-color=always --width=240 --syntax-theme=zenburn | \
    aha --black | \
    tail -n+2
