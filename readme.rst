
##########
Git to RSS
##########

Command line utility to extract an RSS feed from a Git repository,
so commits can be reviewed in new-readers or email clients.

Usage
=====

.. code:: sh

   git_to_rss --git-dir=/source/blender --url=https://developer.blender.org/rB --git-after=2.weeks.ago --rss-out output.rss


Features
========

- Cache result for fast updates.
- Uses multi-processing to access commits.
- Special handing of Git-SVN repositories.
- Experimental ``FUSE`` support (for applications that require RSS feeds to reference a file/URL).
